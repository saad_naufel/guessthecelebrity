//
//  InAppPurchaseViewController.swift
//  GuessTheCelebrity
//
//  Created by Saad on 4/19/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class InAppPurchaseViewController: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {

    @IBOutlet var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var admobBanner: GADBannerView!
    @IBOutlet var bundleOfCoinButton: UIButton!
    @IBOutlet var bunchOfCoinButton: UIButton!
    @IBOutlet var packOfCoinButton: UIButton!
    @IBOutlet var grabCoinTopConstraint: NSLayoutConstraint!
    @IBOutlet var grabCoinButton: UIButton!
    @IBOutlet var coinCountButton: UIButton!
    
    @IBOutlet var BagOfCoinButton: UIButton!
    
    @IBOutlet var packOfCoinWidth: NSLayoutConstraint!
    
    @IBOutlet var bunchOfCoinWidth: NSLayoutConstraint!
    
    @IBOutlet var bagOfCoinWidth: NSLayoutConstraint!
    @IBOutlet var bundleOfCoinWidth: NSLayoutConstraint!
    var screenWidth, screenHeight: CGFloat!
    var products = [SKProduct]()
    var busyView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenWidth = UIScreen.mainScreen().bounds.size.width
        screenHeight = UIScreen.mainScreen().bounds.size.height
        
        // Do any additional setup after loading the view.
        grabCoinTopConstraint.constant = UIScreen.mainScreen().bounds.size.height * 0.12
        
        busyView = UIActivityIndicatorView(frame: CGRectMake(0.0, 0.0, UIScreen.mainScreen().bounds.size.width, screenHeight))
        busyView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        busyView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        self.view.addSubview(busyView)
        
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        
        busyView.startAnimating()
        
        
        requestProductInformation()
    }
    
    override func viewWillAppear(animated: Bool) {
        let packOfCoinImage = UIImage(named: "pack_of_coin.png")
        let ratio = (packOfCoinImage?.size.width)! / (packOfCoinImage?.size.height)!
        var tempWidth = ratio * packOfCoinButton.frame.size.height
        if tempWidth >= screenWidth {
            tempWidth = screenWidth * 0.8
        }
        packOfCoinWidth.constant = tempWidth
        bunchOfCoinWidth.constant = tempWidth
        bagOfCoinWidth.constant = tempWidth
        bundleOfCoinWidth.constant = tempWidth
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let commonInfoFetchRequest = NSFetchRequest(entityName: "CommonInfo")
        do {
            let commonInfoResults = try managedContext.executeFetchRequest(commonInfoFetchRequest)
            let commonInfo = commonInfoResults[0]
            let totalCoins = commonInfo.valueForKey("coinCount") as! Int
            coinCountButton.setTitle("\(totalCoins)", forState: UIControlState.Normal)
            
            let hasPurchased = commonInfo.valueForKey("hasPurchased") as! Bool
            if hasPurchased == true {
                admobBanner.hidden = true
                bottomLayoutConstraint.constant = 0.0
            }
            else {
                bottomLayoutConstraint.constant = 50.0
                admobBanner.hidden = false
                admobBanner.adUnitID = ADMOB_AD_UNIT_ID
                admobBanner.rootViewController = self
                admobBanner.loadRequest(GADRequest())
            }
        } catch {
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



    @IBAction func backButtonTapped(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func requestProductInformation() {
        if SKPaymentQueue.canMakePayments() {
            products = [SKProduct]()
            let productIDs = Set(["GuessCelebMovie01", "GuessCelebMovie02", "GuessCelebMovie03", "GuessCelebMovie04"])
            let productRequest = SKProductsRequest(productIdentifiers: productIDs)
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            showAlert("", alertMessage: "In app purchase disabled")
        }
    }
    
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        busyView.stopAnimating()
        products = response.products
        if products.count == 0 {
            showAlert("", alertMessage: "No products found")
        }
    }
    
    func request(request: SKRequest, didFailWithError error: NSError) {
        busyView.stopAnimating()
        showAlert("", alertMessage: error.localizedDescription)
    }
    
    @IBAction func productButtonPressed(sender: UIButton) {
        if products.count == 0 {
            showAlert("", alertMessage: "No products to purchase")
        }
        else {
            busyView.startAnimating()
            var index = 0
            for i in 0 ..< products.count {
                if sender.tag == 1000 && products[i].productIdentifier == "GuessCelebMovie01" {
                    index = i
                    break
                }
                if sender.tag == 2500 && products[i].productIdentifier == "GuessCelebMovie02" {
                    index = i
                    break
                }
                if sender.tag == 4500 && products[i].productIdentifier == "GuessCelebMovie03" {
                    index = i
                    break
                }
                if sender.tag == 9500 && products[i].productIdentifier == "GuessCelebMovie04" {
                    index = i
                    break
                }
            }
            let payment = SKPayment(product: products[index])
            SKPaymentQueue.defaultQueue().addPayment(payment)
        }
    }
    
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions as [SKPaymentTransaction] {
            switch transaction.transactionState {
            case SKPaymentTransactionState.Purchased:
                if transaction.payment.productIdentifier == "GuessCelebMovie01" {
                    deliverProduct(1000)
                }
                else if transaction.payment.productIdentifier == "GuessCelebMovie02" {
                    deliverProduct(2500)
                }
                else if transaction.payment.productIdentifier == "GuessCelebMovie03" {
                    deliverProduct(4500)
                }
                else if transaction.payment.productIdentifier == "GuessCelebMovie04" {
                    deliverProduct(9500)
                }
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                busyView.stopAnimating()
                
            case SKPaymentTransactionState.Failed:
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                showAlert("", alertMessage: "Transaction failed")
                busyView.stopAnimating()
                
            default:
                break
            }
        }
    }
    
    func deliverProduct(coinIncrement: Int) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let commonInfoFetchRequest = NSFetchRequest(entityName: "CommonInfo")
        do {
            let commonInfoResults = try managedContext.executeFetchRequest(commonInfoFetchRequest)
            let commonInfo = commonInfoResults[0]
            var totalCoins = commonInfo.valueForKey("coinCount") as! Int
            totalCoins += coinIncrement
            commonInfo.setValue(totalCoins, forKey: "coinCount")
            commonInfo.setValue(true, forKey: "hasPurchased")
            try managedContext.save()
            coinCountButton.setTitle("\(totalCoins)", forState: UIControlState.Normal)
            admobBanner.hidden = true
            bottomLayoutConstraint.constant = 0.0
        } catch {
            
        }
    }
    
    func showAlert(alertTitle: String, alertMessage: String) {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}
