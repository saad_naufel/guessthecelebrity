//
//  LevelViewController.swift
//  GuessTheCelebrity
//
//  Created by Saad on 4/11/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class LevelViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var admobBanner: GADBannerView!
    @IBOutlet var selectLevelButton: UIButton!
    @IBOutlet var levelInfoTable: UITableView!
    @IBOutlet var coinCountButton: UIButton!
    @IBOutlet weak var levelTopConstraint: NSLayoutConstraint!
    var levelImageNamesUnlock = ["level_1.png", "level_2.png", "level_3.png", "level_4.png", "level_5.png", "level_6.png", "level_7.png"]
    
    var levelImageNamesLock = ["level_1_locked.png", "level_2_locked.png", "level_3_locked.png", "level_4_locked.png", "level_5_locked.png", "level_6_locked.png", "level_7_locked.png"]
    
    var levelLockedStatus = [Bool]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        levelTopConstraint.constant = UIScreen.mainScreen().bounds.size.height * 0.12
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        selectLevelButton.titleLabel?.font = UIFont(name: "Savoye LET", size: selectLevelButton.frame.size.height * 0.8)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let commonInfoFetchRequest = NSFetchRequest(entityName: "CommonInfo")
        let levelInfoFetchRequest = NSFetchRequest(entityName: "LevelInfo")
        do {
            let commonInfoResults = try managedContext.executeFetchRequest(commonInfoFetchRequest)
            let commonInfo = commonInfoResults[0]
            let totalCoins = commonInfo.valueForKey("coinCount") as! Int
            coinCountButton.setTitle("\(totalCoins)", forState: UIControlState.Normal)
            
            let hasPurchased = commonInfo.valueForKey("hasPurchased") as! Bool
            if hasPurchased == true {
                admobBanner.hidden = true
                bottomLayoutConstraint.constant = 0.0
            }
            else {
                bottomLayoutConstraint.constant = 50.0
                admobBanner.hidden = false
                admobBanner.adUnitID = ADMOB_AD_UNIT_ID
                admobBanner.rootViewController = self
                admobBanner.loadRequest(GADRequest())
            }
            
            for _ in 0 ..< 7 {
                levelLockedStatus.append(false)
            }
            let levelInfoResults = try managedContext.executeFetchRequest(levelInfoFetchRequest)
            let levelInfo = levelInfoResults as! [NSManagedObject]
            for i in 0 ..< 7 {
                let locked = levelInfo[i].valueForKey("isLocked") as! Bool
                let levelNo = levelInfo[i].valueForKey("levelNo") as! Int
                levelLockedStatus[levelNo - 1] = locked
            }
            levelInfoTable.reloadData()
            
        } catch {
            
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 7
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("levelTableViewCell", forIndexPath: indexPath) as! LevelTableViewCell
        cell.backgroundColor = UIColor.clearColor()
        if indexPath.row % 2 == 0 {
            cell.leftButton.hidden = false
            
            
            if levelLockedStatus[indexPath.row] == true {
                cell.leftButton.userInteractionEnabled = false
                cell.leftButton.setBackgroundImage(UIImage(named: levelImageNamesLock[indexPath.row]), forState: UIControlState.Normal)
            }
            else
            {
                cell.leftButton.userInteractionEnabled = true
                cell.leftButton.setBackgroundImage(UIImage(named: levelImageNamesUnlock[indexPath.row]), forState: UIControlState.Normal)
            }
            
            cell.leftButton.setTitle("LEVEL - \(indexPath.row + 1)", forState: UIControlState.Normal)
            cell.leftButton.tag = indexPath.row
            cell.rightButton.hidden = true
        }
        else
        {
            cell.leftButton.hidden = true
            
            
            if levelLockedStatus[indexPath.row] == true {
                cell.rightButton.userInteractionEnabled = false
                cell.rightButton.setBackgroundImage(UIImage(named: levelImageNamesLock[indexPath.row]), forState: UIControlState.Normal)
            }
            else
            {
                cell.rightButton.userInteractionEnabled = true
                cell.rightButton.setBackgroundImage(UIImage(named: levelImageNamesUnlock[indexPath.row]), forState: UIControlState.Normal)
            }
            cell.rightButton.setTitle("LEVEL - \(indexPath.row + 1)", forState: UIControlState.Normal)
            cell.rightButton.tag = indexPath.row
            cell.rightButton.hidden = false
        }
        
    
        return cell
        
    }
    @IBAction func levelButtonTapped(sender: UIButton) {
        let destinationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StageSelect") as! StageViewController
        destinationViewController.levelNo = sender.tag + 1
        
        self.navigationController?.pushViewController(destinationViewController, animated: true)
    }

    @IBAction func backButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
