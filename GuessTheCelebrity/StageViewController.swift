//
//  StageViewController.swift
//  GuessTheCelebrity
//
//  Created by Saad on 4/12/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class StageViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    @IBOutlet var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var admobBanner: GADBannerView!
    @IBOutlet var coinCountButton: UIButton!
    @IBOutlet var StageCollectionView: UICollectionView!
    @IBOutlet var levelNoButton: UIButton!

    @IBOutlet var StageTopConstraint: NSLayoutConstraint!
    var screenWidth, screenHeight: CGFloat!
    var levelNo: Int!
    var stageSolveStates = [Bool]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenWidth = UIScreen.mainScreen().bounds.size.width
        screenHeight = UIScreen.mainScreen().bounds.size.height
        levelNoButton.setTitle("Level - \(levelNo)", forState: UIControlState.Normal)

        // Do any additional setup after loading the view.
        StageTopConstraint.constant = UIScreen.mainScreen().bounds.size.height * 0.12
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        levelNoButton.titleLabel?.font = UIFont(name: "Savoye LET", size: levelNoButton.frame.size.height * 0.8)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let commonInfoFetchRequest = NSFetchRequest(entityName: "CommonInfo")
        let stageInfoFetchRequest = NSFetchRequest(entityName: "StageInfo")
        let stagePredicate = NSPredicate(format: "levelNo == %i", levelNo)
        stageInfoFetchRequest.predicate = stagePredicate
        do {
            let commonInfoResults = try managedContext.executeFetchRequest(commonInfoFetchRequest)
            let commonInfo = commonInfoResults[0]
            let totalCoins = commonInfo.valueForKey("coinCount") as! Int
            coinCountButton.setTitle("\(totalCoins)", forState: UIControlState.Normal)
            
            let hasPurchased = commonInfo.valueForKey("hasPurchased") as! Bool
            if hasPurchased == true {
                admobBanner.hidden = true
                bottomLayoutConstraint.constant = 0.0
            }
            else {
                bottomLayoutConstraint.constant = 50.0
                admobBanner.hidden = false
                admobBanner.adUnitID = ADMOB_AD_UNIT_ID
                admobBanner.rootViewController = self
                admobBanner.loadRequest(GADRequest())
            }
            
            for _ in 0 ..< 20 {
                stageSolveStates.append(false)
            }
            let stageInfoResults = try managedContext.executeFetchRequest(stageInfoFetchRequest)
            var stageSolveCount = 0
            for stage in stageInfoResults {
                let stageNo = stage.valueForKey("stageNo")as! Int
                let solved = stage.valueForKey("isSolved") as! Bool
                if solved == true
                {
                    stageSolveCount += 1
                }
                stageSolveStates[stageNo - 1] = solved
                
                if stageSolveCount == 20
                {
                    let levelInfoFetchRequest = NSFetchRequest(entityName: "LevelInfo")
                    let levelPredicate = NSPredicate(format: "levelNo == %i", levelNo+1)
                    levelInfoFetchRequest.predicate = levelPredicate
                    if levelNo < 7
                    {
                        let levelInfoResults = try managedContext.executeFetchRequest(levelInfoFetchRequest)
                        let levelLock = levelInfoResults[0].valueForKey("isLocked") as! Bool
                        if levelLock == true
                        {
                            levelInfoResults[0].setValue(false, forKey: "isLocked")
                            try managedContext.save()
                            let alert = UIAlertController(title: "Level unlocked", message: "A new level has been unlocked : Level \(levelNo+1)", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                            presentViewController(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
            StageCollectionView.reloadData()
            
        } catch {
            
        }
    }
    

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSizeMake((screenWidth * 0.85) / 4.0, ((screenHeight * 0.82) - 50.0) / 5.0)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("StageCollectionCell", forIndexPath: indexPath) as! StageCollectionViewCell
        if stageSolveStates[indexPath.item] == true {
            cell.StageSelectButton.setBackgroundImage(UIImage(named: "stage_number_solved.png"), forState: UIControlState.Normal)
            cell.StageSelectButton.setTitleColor(UIColor(red: 45.0/255.0, green: 62.0/255.0, blue: 79.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
            
        }
        else
        {
            cell.StageSelectButton.setBackgroundImage(UIImage(named: "stage_number.png"), forState: UIControlState.Normal)
            cell.StageSelectButton.setTitleColor(UIColor(red: 223.0/255.0, green: 220.0/255.0, blue: 145.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        }
        
        cell.StageSelectButton.setTitle("\(indexPath.item + 1)", forState: UIControlState.Normal)
        cell.StageSelectButton.tag = indexPath.item + 1
        cell.StageSelectButton.titleLabel?.font = UIFont(name: "Myriad Pro", size: cell.StageSelectButton.frame.size.height * 0.5)
        
        return cell
    }
    @IBAction func StageBackButton(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func stageSelectTap(sender: UIButton) {
        let destinationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GameplayView") as! GameplayViewController
        destinationViewController.levelNo = levelNo
        destinationViewController.stageNo = sender.tag
        destinationViewController.hasSolvedBefore = stageSolveStates[sender.tag - 1]
        
        self.navigationController?.pushViewController(destinationViewController, animated: true)
    }
    
}
