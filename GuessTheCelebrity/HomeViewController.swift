//
//  HomeViewController.swift
//  GuessTheCelebrity
//
//  Created by Saad on 4/11/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import CoreData
import Social

class HomeViewController: UIViewController {

    @IBOutlet var appTitleButton: UIButton!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var toggleSound: UIButton!
    var levelInfo = [NSManagedObject]()
    var playSound: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        playButton.titleLabel?.font = UIFont(name: "Savoye LET", size: playButton.frame.size.height * 0.65)
        appTitleButton.titleLabel?.font = UIFont(name: "Savoye LET", size: appTitleButton.frame.size.height * 0.65)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "CommonInfo")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            playSound = results[0].valueForKey("playSound") as! Bool
        }
        catch {
            
        }
        if playSound == true {
            toggleSound.setBackgroundImage(UIImage(named: "btn-sound-on.png"), forState: UIControlState.Normal)
        }
        else {
            toggleSound.setBackgroundImage(UIImage(named: "btn-sound-off.png"), forState: UIControlState.Normal)
        }
    }
    
    func initData() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "LevelInfo")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            levelInfo = results as! [NSManagedObject]
            if levelInfo.count == 0 {
                let commonInfoEntity = NSEntityDescription.entityForName("CommonInfo", inManagedObjectContext: managedContext)
                let commonInfo = NSManagedObject(entity: commonInfoEntity!, insertIntoManagedObjectContext: managedContext)
                commonInfo.setValue(1000, forKey: "coinCount")
                //Need to be set into false
                commonInfo.setValue(false, forKey: "hasPurchased")
                commonInfo.setValue(true, forKey: "playSound")
                let levelEntity = NSEntityDescription.entityForName("LevelInfo", inManagedObjectContext: managedContext)
                for i in 1...7 {
                    let level = NSManagedObject(entity: levelEntity!, insertIntoManagedObjectContext: managedContext)
                    level.setValue(i, forKey: "levelNo")
                    if i == 1 {
                        level.setValue(false, forKey: "isLocked")
                    }
                    else {
                        level.setValue(true, forKey: "isLocked")
                    }
                    let stageEntity = NSEntityDescription.entityForName("StageInfo", inManagedObjectContext: managedContext)
                    for j in 1...20 {
                        let stage = NSManagedObject(entity: stageEntity!, insertIntoManagedObjectContext: managedContext)
                        stage.setValue(i, forKey: "levelNo")
                        stage.setValue(j, forKey: "stageNo")
                        stage.setValue(false, forKey: "isSolved")
                    }
                }
                do {
                    try managedContext.save()
                } catch let error as NSError {
                    print("could not save \(error.userInfo)")
                }
            }
        } catch {
            abort()
        }
    }

    func clearGameProgressData() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let levelInfoFetchRequest = NSFetchRequest(entityName: "LevelInfo")
        let stageInfoFetchRequest = NSFetchRequest(entityName: "StageInfo")
        do {
            let levelInfoResults = try managedContext.executeFetchRequest(levelInfoFetchRequest)
            for i in 0 ..< levelInfoResults.count {
                let levelNo = levelInfoResults[i].valueForKey("levelNo") as! Int
                if levelNo == 1 {
                    levelInfoResults[i].setValue(false, forKey: "isLocked")
                }
                else {
                    levelInfoResults[i].setValue(true, forKey: "isLocked")
                }
            }
            let stageInfoResults = try managedContext.executeFetchRequest(stageInfoFetchRequest)
            for i in 0 ..< stageInfoResults.count {
                stageInfoResults[i].setValue(false, forKey: "isSolved")
            }
            try managedContext.save()
        }
        catch {
            
        }
    }
    
    @IBAction func likeAppTapped(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string: APP_STORE_LINK)!)
    }
    
    @IBAction func toggleSoundTapped(sender: UIButton) {
        
        if playSound == true {
            playSound = false
            toggleSound.setBackgroundImage(UIImage(named: "btn-sound-off.png"), forState: UIControlState.Normal)
        }
        else {
            playSound = true
            toggleSound.setBackgroundImage(UIImage(named: "btn-sound-on.png"), forState: UIControlState.Normal)
        }
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "CommonInfo")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            results[0].setValue(playSound, forKey: "playSound")
            try managedContext.save()
        }
        catch {
            
        }
    }

    @IBAction func restorePurchaseTapped(sender: UIButton) {
        let alert = UIAlertController(title: nil, message: "All game progress will be lost. Are you sure?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: {(UIAlertAction) -> Void in
            self.clearGameProgressData()}))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func fbShareTapped(sender: UIButton) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            let facebookSheet = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.addURL(NSURL(string: APP_STORE_LINK))
            self.presentViewController(facebookSheet, animated: true, completion: nil)
        }
    }
}
