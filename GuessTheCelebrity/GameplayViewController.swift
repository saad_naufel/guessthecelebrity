//
//  GameplayViewController.swift
//  GuessTheCelebrity
//
//  Created by Saad on 4/13/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData
import Social
import GoogleMobileAds

class GameplayViewController: UIViewController {
    @IBOutlet var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var admobBanner: GADBannerView!
    @IBOutlet var CoinCountButton: UIButton!
    @IBOutlet var nameHolder: UIView!
    @IBOutlet var gameImage: UIImageView!
    @IBOutlet var nameHolderHeight: NSLayoutConstraint!
    @IBOutlet var OptionHolder: UIView!
    @IBOutlet var OptionHolderHeight: NSLayoutConstraint!
    var celebrityName: String!
    var options = [String]()
    var screenWidth, screenHeight: CGFloat!
    var stageNo, levelNo: Int!
    var nameButtonSize: CGFloat!
    var nameHasOption = [Int]()
    var totalCoins: Int!
    var playSound: Bool!
    var hasSolvedBefore: Bool!
    var tap: AVAudioPlayer?
    var reverseTap: AVAudioPlayer?
    var success: AVAudioPlayer?
    var hasPurchased: Bool!
    var busyView: UIActivityIndicatorView!
    
    @IBOutlet var successView: UIView!
    @IBOutlet var gameplayTopConstraint: NSLayoutConstraint!
    @IBOutlet var deductButton: UIButton!
    @IBOutlet var singleSolveButton: UIButton!
    
    @IBOutlet var solveButton: UIButton!
    @IBOutlet var fbShare: UIButton!
    @IBOutlet var stageTopConstraint: NSLayoutConstraint!
    @IBOutlet var stageNoButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        successView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        successView.hidden =  true
        
        celebrityName = celebNames[levelNo - 1][stageNo - 1]
        screenWidth = UIScreen.mainScreen().bounds.size.width
        screenHeight = UIScreen.mainScreen().bounds.size.height
        nameHasOption = [Int]()
        generateOptions()
        addOptionButtons()
        addNameButtons()
        
        let imageName = "l\(levelNo)s\(stageNo).jpg"
        gameImage.layer.borderWidth = 5.0
        gameImage.layer.borderColor = UIColor(red: 4.0/255.0, green: 129.0/255.0, blue: 155.0/255.0, alpha: 1.0).CGColor
        gameImage.image = UIImage(named: imageName)
        
        deductButton.layer.cornerRadius = (screenHeight * 0.1 * 0.7) / 4.0
        singleSolveButton.layer.cornerRadius = (screenHeight * 0.1 * 0.7) / 4.0
        solveButton.layer.cornerRadius = (screenHeight * 0.1 * 0.7) / 4.0
        fbShare.layer.cornerRadius = (screenHeight * 0.1 * 0.7) / 4.0
        stageNoButton.setTitle("Stage - \(stageNo)", forState: UIControlState.Normal)
        gameplayTopConstraint.constant = UIScreen.mainScreen().bounds.size.height * 0.12
        
        var path = NSBundle.mainBundle().pathForResource("character_tap", ofType: "wav")
        let tapURL = NSURL(fileURLWithPath: path!)
        path = NSBundle.mainBundle().pathForResource("character_tap_reverse", ofType: "wav")
        let reverseTapURL = NSURL(fileURLWithPath: path!)
        path = NSBundle.mainBundle().pathForResource("success", ofType: "wav")
        let successURL = NSURL(fileURLWithPath: path!)
        do {
            try tap = AVAudioPlayer(contentsOfURL: tapURL)
            try reverseTap = AVAudioPlayer(contentsOfURL: reverseTapURL)
            try success = AVAudioPlayer(contentsOfURL: successURL)
        } catch {
            
        }
        
        busyView = UIActivityIndicatorView(frame: CGRectMake(0.0, 0.0, UIScreen.mainScreen().bounds.size.width, screenHeight))
        busyView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        busyView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        successView.addSubview(busyView)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        stageNoButton.titleLabel?.font = UIFont(name: "Savoye LET", size: stageNoButton.frame.size.height * 0.8)
        deductButton.titleLabel?.font = UIFont(name: "Myriad Pro", size: deductButton.frame.size.height * 0.5)
        singleSolveButton.titleLabel?.font = UIFont(name: "Myriad Pro", size: singleSolveButton.frame.size.height * 0.5)
        solveButton.titleLabel?.font = UIFont(name: "Myriad Pro", size: solveButton.frame.size.height * 0.5)
        fbShare.titleLabel?.font = UIFont(name: "Myriad Pro", size: fbShare.frame.size.height * 0.4)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let commonInfoFetchRequest = NSFetchRequest(entityName: "CommonInfo")
        do {
            let commonInfoResults = try managedContext.executeFetchRequest(commonInfoFetchRequest)
            let commonInfo = commonInfoResults[0]
            totalCoins = commonInfo.valueForKey("coinCount") as! Int
            CoinCountButton.setTitle("\(totalCoins)", forState: UIControlState.Normal)
            playSound = commonInfo.valueForKey("playSound") as! Bool
            
            hasPurchased = commonInfo.valueForKey("hasPurchased") as! Bool
            if hasPurchased == true {
                admobBanner.hidden = true
                bottomLayoutConstraint.constant = 0.0
            }
            else {
                bottomLayoutConstraint.constant = 50.0
                admobBanner.hidden = false
                admobBanner.adUnitID = ADMOB_AD_UNIT_ID
                admobBanner.rootViewController = self
                admobBanner.loadRequest(GADRequest())
            }
        } catch {
            
        }
    }

    func generateOptions() {
        var trimmedName = celebrityName.stringByReplacingOccurrencesOfString(" ", withString: "")
        let characterSet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
        let remainingCharacters = 16 - trimmedName.characters.count
        for _ in 0 ..< remainingCharacters {
            let randomIndex = Int(arc4random_uniform(UInt32(characterSet.count)))
            trimmedName = trimmedName + characterSet[randomIndex]
        }
        var index = 0
        for ch in trimmedName.characters {
            var str = ""
            str.append(ch)
            options.insert(str, atIndex: index)
            index += 1
        }
        for i in 0 ..< options.count {
            let randomIndex = Int(arc4random_uniform(16))
            let tmp = options[i]
            options[i] = options[randomIndex]
            options[randomIndex] = tmp
        }
    }
    
    func addOptionButtons() {
        let optionButtonSize = screenWidth * 0.1025
        let optionButtonSpacing = screenWidth * 0.02
        OptionHolderHeight.constant = (optionButtonSize * 2.0) + 24.0
        var x = optionButtonSpacing
        var y:CGFloat = 8.0
        var buttonTag = 2000
        var optionIndex = 0
        for _ in 0 ..< 2 {
            for _ in 0 ..< 8 {
                let optionButton = UIButton(frame: CGRectMake(x, y, optionButtonSize, optionButtonSize))
                optionButton.setBackgroundImage(UIImage(named: "character_tap_holder.png"), forState: UIControlState.Normal)
                optionButton.titleLabel?.font = UIFont(name: "Phosphor CG", size: optionButtonSize*0.6)
                optionButton.setTitle(options[optionIndex], forState: UIControlState.Normal)
                optionIndex += 1
                optionButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                optionButton.tag = buttonTag
                buttonTag += 1
                optionButton.addTarget(self, action: #selector(GameplayViewController.optionButtonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                OptionHolder.addSubview(optionButton)
                x = x + optionButtonSize + optionButtonSpacing
            }
            x = optionButtonSpacing
            y = y + optionButtonSize + 8.0
        }
    }
    
    func optionButtonPressed(sender: UIButton) {
        if playSound == true {
            tap?.play()
        }
        let trimmedName = celebrityName.stringByReplacingOccurrencesOfString(" ", withString: "")
        var gussedName = ""
        var isFirstBlank = true
        let optionTitle = sender.titleLabel?.text
        for subView in nameHolder.subviews {
            if subView.isKindOfClass(UIButton) {
                let button = subView as! UIButton
                if nameHasOption[button.tag - 1000] == 0  && isFirstBlank == true {
                    button.setTitle(optionTitle, forState: UIControlState.Normal)
                    gussedName = gussedName + optionTitle!
                    nameHasOption[button.tag - 1000] = sender.tag
                    sender.hidden = true
                    isFirstBlank = false
                }
                else if nameHasOption[button.tag - 1000] > 0 {
                    gussedName = gussedName + (button.titleLabel?.text)!
                }
            }
        }
        if gussedName.characters.count == trimmedName.characters.count {
            if gussedName == trimmedName {
                stageSolved()
            }
            else {
                showAlertWithMessage("", alertMessage: "Wrong guess. Try another one")
            }
        }
    }
    
    func stageSolved() {
        
        
        if hasSolvedBefore == false {
            changeCoinAmount(STAGE_COMPLETION_COIN_AMOUNT, plus: true, minus: false)
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            let stageInfoFetchRequest = NSFetchRequest(entityName: "StageInfo")
            let stagePredicate = NSPredicate(format: "(levelNo == %i) AND (stageNo == %i)", levelNo, stageNo)
            stageInfoFetchRequest.predicate = stagePredicate
            do {
                let stageInfoResults = try managedContext.executeFetchRequest(stageInfoFetchRequest)
                stageInfoResults[0].setValue(true, forKey: "isSolved")
                try managedContext.save()
            }
            catch {
                
            }
            hasSolvedBefore = true
        }
        successView.hidden = false
        
        if playSound == true {
            success?.play()
        }
    }
    
    func changeCoinAmount(changeAmount: Int, plus: Bool, minus: Bool) {
        if plus == true {
            totalCoins = totalCoins + changeAmount
        }
        else if minus == true {
            totalCoins = totalCoins - changeAmount
        }
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let commonInfoFetchRequest = NSFetchRequest(entityName: "CommonInfo")
        do {
            let commonInfoResults = try managedContext.executeFetchRequest(commonInfoFetchRequest)
            let commonInfo = commonInfoResults[0]
            commonInfo.setValue(totalCoins, forKey: "coinCount")
            try managedContext.save()
            let commonInfoResultsAfterSaving = try managedContext.executeFetchRequest(commonInfoFetchRequest)
            let commonInfoAfterSaving = commonInfoResultsAfterSaving[0]
            totalCoins = commonInfoAfterSaving.valueForKey("coinCount") as! Int
            CoinCountButton.setTitle("\(totalCoins)", forState: UIControlState.Normal)
        } catch {
            
        }
    }
    
    func showAlertWithMessage(alertTitle: String, alertMessage: String) {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func addNameButtons() {
        nameButtonSize = (screenWidth - 78.0)/12.0
        var noOfLine = 1
        var nameParts = ["", ""]
        if celebrityName.characters.count > 10 {
            noOfLine = 2
            nameHolderHeight.constant = nameButtonSize * 2.0 + 18.0
            var parts = celebrityName.componentsSeparatedByString(" ")
            if parts.count == 3 {
                nameParts[0] = parts[0]
                nameParts[1] = parts[1] + " " + parts[2]
            }
            else {
                nameParts[0] = parts[0]
                nameParts[1] = parts[1]
            }
        }
        else {
            nameHolderHeight.constant = nameButtonSize + 12.0
            nameParts[0] = celebrityName
        }
        var y: CGFloat = 6.0
        if noOfLine == 1 {
            y = (nameHolderHeight.constant - nameButtonSize) / 2.0
        }
        var buttonTag = 1000
        for i in 0 ..< noOfLine {
            var x: CGFloat = (screenWidth - CGFloat(nameParts[i].characters.count) * nameButtonSize - CGFloat(nameParts[i].characters.count - 1) * 6.0) / 2.0
            for ch in nameParts[i].characters {
                var buttonTitle = ""
                buttonTitle.append(ch)
                if buttonTitle != " " {
                    
                    let nameButton = UIButton(frame: CGRectMake(x, y, nameButtonSize, nameButtonSize))
                    nameButton.setBackgroundImage(UIImage(named: "character_result.png"), forState: UIControlState.Normal)
                    nameButton.setTitleColor(UIColor(red: 45.0 / 255.0, green: 62.0 / 255.0, blue: 79.0 / 255.0, alpha: 1.0), forState: UIControlState.Normal)
                    nameButton.titleLabel?.font = UIFont(name: "Myriad Pro", size: nameButtonSize * 0.7)
                    nameButton.setTitle(nil, forState: UIControlState.Normal)
                    nameButton.tag = buttonTag
                    nameButton.addTarget(self, action: #selector(GameplayViewController.nameButtonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                    buttonTag += 1
                    nameHolder.addSubview(nameButton)
                    nameHasOption.append(0)
                }
                x = x + nameButtonSize + 6.0
            }
            y = y + nameButtonSize + 6.0
        }
    }
    
    func nameButtonPressed(sender: UIButton) {
        if playSound == true {
            reverseTap?.play()
        }
        if nameHasOption[sender.tag - 1000] > 0 {
            sender.setTitle("", forState: UIControlState.Normal)
            let optionView = OptionHolder.viewWithTag(nameHasOption[sender.tag - 1000])
            optionView?.hidden = false
            nameHasOption[sender.tag - 1000] = 0
        }
    }

    
    
    @IBAction func deductButtonTapped(sender: UIButton) {
        
        if totalCoins >= ELIMINATE_COIN_AMOUNT {
            let trimmedName = celebrityName.stringByReplacingOccurrencesOfString(" ", withString: "")
            var extraOptions = [Int]()
            for subView in OptionHolder.subviews {
                if subView.isKindOfClass(UIButton) == true {
                    let button = subView as! UIButton
                    let buttonTitle = button.titleLabel?.text
                    if trimmedName.containsString(buttonTitle!) == false {
                        extraOptions.append(button.tag)
                    }
                }
            }
            if extraOptions.count == 0 {
                showAlertWithMessage("", alertMessage: "Nothing to eliminate. All the options could be useful")
            }
            else {
                let eliminationCount = Int(ceil(CGFloat(extraOptions.count) / 2.0))
                for _ in 0 ..< eliminationCount {
                    let index = Int(arc4random_uniform(UInt32(extraOptions.count)))
                    let subView = OptionHolder.viewWithTag(extraOptions[index])
                    subView?.removeFromSuperview()
                    extraOptions.removeAtIndex(index)
                }
                changeCoinAmount(ELIMINATE_COIN_AMOUNT, plus: false, minus: true)
            }
        }
        else {
            showAlertWithMessage("", alertMessage: "You do not have enough coins")
        }
    }
    
    @IBAction func stageSolveButtonTapped(sender: UIButton) {
        if totalCoins >= SOLVE_COIN_AMOUNT {
            let trimmedName = celebrityName.stringByReplacingOccurrencesOfString(" ", withString: "")
            var index = 0
            for ch in trimmedName.characters {
                var buttonTitle = ""
                buttonTitle.append(ch)
                let button = nameHolder.viewWithTag(1000+index) as! UIButton
                button.setTitle(buttonTitle, forState: UIControlState.Normal)
                index += 1
            }
            changeCoinAmount(SOLVE_COIN_AMOUNT, plus: false, minus: true)
            stageSolved()
        }
        else {
            showAlertWithMessage("", alertMessage: "You do not have enough coins")
        }
    }
    @IBAction func singleSolveButtonTapped(sender: UIButton) {
        
        if totalCoins >= REVEAL_COIN_AMOUNT {
            for subView in nameHolder.subviews {
                if subView.isKindOfClass(UIButton) == true {
                    let button = subView as! UIButton
                    button.setTitle("", forState: UIControlState.Normal)
                    nameHasOption[button.tag - 1000] = 0
                }
            }
            for subView in OptionHolder.subviews {
                if subView.isKindOfClass(UIButton) == true {
                    subView.hidden = false
                }
            }
            let trimmedName = celebrityName.stringByReplacingOccurrencesOfString(" ", withString: "")
            let revealCount = Int(ceil(CGFloat(trimmedName.characters.count) / 2.0))
            var randomPosition = 0
            for _ in 0 ..< revealCount {
                while true {
                    randomPosition = Int(arc4random_uniform(UInt32(trimmedName.characters.count)))
                    if nameHasOption[randomPosition] == 0 {
                        var index = 0
                        var buttonTitle = ""
                        for ch in trimmedName.characters {
                            if index == randomPosition {
                                buttonTitle.append(ch)
                                break
                            }
                            index += 1
                        }
                        for subView in OptionHolder.subviews {
                            if subView.isKindOfClass(UIButton) == true {
                                let button = subView as! UIButton
                                if button.titleLabel?.text == buttonTitle {
                                    nameHasOption[randomPosition] = button.tag
                                    let nameButton = (nameHolder.viewWithTag(randomPosition + 1000)) as! UIButton
                                    nameButton.setTitle(buttonTitle, forState: UIControlState.Normal)
                                    button.hidden = true
                                    break
                                }
                            }
                        }
                        break
                    }
                }
            }
            changeCoinAmount(REVEAL_COIN_AMOUNT, plus: false, minus: true)
        }
        else {
            showAlertWithMessage("", alertMessage: "You do not have enough coins")
        }
        
    }
    
    @IBAction func fbShareButtonTapped(sender: UIButton) {
        
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            let fbShare = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            fbShare.setInitialText("Can you guess this celebrity by this picture?")
            let imageName = "l\(levelNo)s\(stageNo).jpg"
            fbShare.addURL(NSURL(string: APP_STORE_LINK))
            fbShare.addImage(UIImage(named: imageName))
            
            self.presentViewController(fbShare, animated: true, completion: nil)
            
            
        }
    }
    
    @IBAction func gameplayBackButton(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func gameSuccessContinueTapped(sender: UIButton) {
        
        if hasPurchased == true {
            self.navigationController?.popViewControllerAnimated(true)
        }
        else {
            if revmobSessionStarted == true {
                if let fullScreenAd = RevMobAds.session().fullscreen() {
                    dispatch_async(dispatch_get_main_queue(), {
                        self.busyView.startAnimating()
                    })
                    fullScreenAd.loadWithSuccessHandler({(fs: RevMobFullscreen!) -> Void in
                        fs.showAd()}, andLoadFailHandler: {(fs: RevMobFullscreen!, error: NSError!) -> Void in
                            self.navigationController?.popViewControllerAnimated(true)}, onClickHandler: nil, onCloseHandler: {() -> Void in
                                self.navigationController?.popViewControllerAnimated(true)
                    })
                }
                else {
                    self.navigationController?.popViewControllerAnimated(true)
                }
            }
            else {
                self.navigationController?.popViewControllerAnimated(true)
            }
        }
    
    }
    
}
